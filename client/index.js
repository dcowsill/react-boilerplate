import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';
import Timer from './components/Timer.jsx';

ReactDOM.render(<Timer />, document.getElementById('root'));
